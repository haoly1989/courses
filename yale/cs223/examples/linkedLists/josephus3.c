#include <stdlib.h>
#include <stdio.h>
#include "list.h"

/**
 * Josephus问题求解：接口版
 * 约定：
 * - 客户端程序不直接引用链表节点，但可以通过函数调用来引用链表节点。
 * - 客户端程序通过函数调用获取到的节点包含自引用。
 * 
 * 编译：
 * gcc -o josephus3 josephus3.c linkedList.c
 * 运行：
 * ./josephus3 9 5
 * 输出：
 * 8
 */
int main(int argc, char **argv){
    if (argc != 3) {
        fprintf(stderr, "Usage: %s N M\n", argv[0]);
        return 1;
    }   

    int i;
    int N = atoi(argv[1]);
    int M = atoi(argv[2]);

    Node t;//指向待插入节点
    Node x;//指向待插入节点的前一个节点
    initNodes(N);
    x = newNode(1);
    for (i = 2; i <= N; i++) {
        t = newNode(i);
        insertNext(x, t);
        x = t;
    }
    while (x != Next(x)) {
        for (i = 1; i < M; i++) {
            x = Next(x);
        }
        //x指向待删除节点的前一个节点
        freeNode(deleteNext(x));
    }

    printf("%d\n", Item(x));
    return 0;
}