#include <stdio.h>
#include <string.h>
#include "STACK.h"
/**
 * Infix-to-postfix conversion
 * 
 * 思路：
 * 从左向右逐个处理字符：
 * - 当遇到左括号时，请跳过，执行下一次处理；
 * - 当遇到数时，直接输出
 * - 当遇到操作符时，入栈；
 * - 当遇到右括号时，弹出栈顶的操作符，并输出
 * 
 * 编译1：
 * gcc -o infix2Postfix infix2Postfix.c ArrayStack.c
 * 
 * 运行：
 * ./infix2Postfix '(1+2)'
 * 
 * ./infix2Postfix '(5*(((9+8)*(4*6))+7))'
 */
int main(int argc, char **argv){
    char *a = argv[1];
    int i;
    int N = strlen(a);

    STACKinit(N);

    for (i = 0; i < N; i++) {
        if (a[i] == ')') {
            printf("%c ", STACKpop());
        }
        if (a[i] == '+' || a[i] == '*') {
            STACKpush(a[i]);
        }
        if (a[i] >= '0' && a[i] <= '9') {
            printf("%c ", a[i]);
        }
    }
    printf("\n");


    return 0;
}