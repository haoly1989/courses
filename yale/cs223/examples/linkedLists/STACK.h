#include "Item.h"

void STACKinit(int);

int STACKempty();

void STACKpush(Item);

Item STACKpop();