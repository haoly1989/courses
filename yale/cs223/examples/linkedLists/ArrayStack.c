#include <stdlib.h>
#include "STACK.h"

/**
 * 当栈中有N个元素时，这个实现以s[0]、s[1]、...、s[N-1]的形式保存它们，按照插入先后从最早到最近的顺序。
 * 栈顶的元素永远都是s[N-1]。
 * 
 */
static Item *s;
static int N;   //下一个要入栈的元素位置

void STACKinit(int maxN){
    s = malloc(maxN*sizeof(Item));
    N = 0;
}

int STACKempty(){
    return N == 0;
}

void STACKpush(Item item){
    s[N++] = item;
}

Item STACKpop(){
    return s[--N];
}
