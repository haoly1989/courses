typedef struct node* link;

typedef link Node;

void initNodes(int);

Node newNode(int);

void freeNode(Node);

Node deleteNext(Node);

void insertNext(Node x, Node t);

Node Next(Node);

int Item(Node);

