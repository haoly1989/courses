#include <stdio.h>
#include <string.h>
#include "STACK.h"

/**
 * File：`evalPostfixExpression.c`
 * ------------------------------
 * 功能：对后缀表达式求值
 * 首先，读取任何后缀表达式(涉及整数的加法和乘法运算)；
 * 接着，对表达式求值；
 * 最后，输出结果。
 * 
 * 求值思路：
 * - 当遇到操作数时，把它入栈；
 * - 当遇到操作符时，先从栈中连续弹出两个元素，后对这两个元素应用操作符，把得到的结果入栈。
 * 
 * 该程序假设：每个整数后有至少一个空格。
 * 
 * 运行：
 * ./evalPostfixExpression '15 5 *'
 * ./evalPostfixExpression '15 5 +'
 * ./evalPostfixExpression '5 9 8 + 4 6 * * 7 + *'
 * 
 * 整合实现对中缀表达式的求值：
 * ./infix2Postfix '(5*(((9+8)*(4 * 6))+7))' | xargs -I{} ./evalPostfixExpression "'{}'"
 */
int main(int argc, char **argv) {
    char *a = argv[1];
    int i;
    int N = strlen(a);

    STACKinit(N);
    for (i = 0; i < N; i++) {
        if (a[i] == '+') {
            STACKpush(STACKpop() + STACKpop());
        }

        if (a[i] == '*') {
            STACKpush(STACKpop() * STACKpop());
        }

        if (a[i] >= '0' && a[i] <= '9') {
            STACKpush(0);
        }
        while(a[i] >= '0' && a[i] <= '9'){
            STACKpush(10*STACKpop() + (a[i++] - '0'));
        }
    }
    printf("%d\n", STACKpop());
    return 0;
}