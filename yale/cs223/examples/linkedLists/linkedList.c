#include <stdlib.h>
#include "list.h"

struct node {
    int item;
    link next;
};

/**
 * 指针实现链表接口
 * - 预先分配指定数量的节点freelist
 * - 当客户端程序需要分配节点时，就从freelist中删除一个节点，返回给客户端程序
 * - 当客户端程序删除一个节点时，就将它加入到freelist中
 */

Node freelist;

void initNodes(int N){
    int i;

    freelist = malloc((N+1)*sizeof(*freelist));
    for (i = 0; i < N; i++) {
        freelist[i].next = &freelist[i+1];
    }
    freelist[N].next = NULL;
}

Node deleteNext(Node x){
    Node t = x->next;
    x->next = t->next;
    return t;
}

/**
 * 新建一个节点
 * 思路：从freelist中删除一个节点
 * 约定：返回给客户端的节点包含自引用
 */
Node newNode(int i){
    Node x = deleteNext(freelist);
    x->item = i;
    x->next = x;
    return x;
}

void insertNext(Node x, Node t){
    t->next = x->next;
    x->next = t;
}

/**
 * 释放节点x
 * 思路：将其加入到freelist中
 */
void freeNode(Node x){
    insertNext(freelist, x);
}

Node Next(Node x){
    return x->next;
}

int Item(Node x){
    return x->item;
}